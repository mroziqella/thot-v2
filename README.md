# THOT #

Thot jest to system wspomagający rekrutacje i zarządzanie pracownikami, głównymi cechami tego systemu jest dodawanie ofert pracy, poszukiwanie pracowników pod kontem konkretnych ofert, dodawanie osób do bazy danych, wyświetlanie profilu osoby, wyświetlanie historii zatrudnienia danej osoby, podsumowywanie stażu pracy. Dodatkowo wyszukiwarki po konkretnych kryteriach(atrybutach) pisujących każdego z pracowników, możliwość składnia wielu takich atrybutów w wyszukiwarce, status kontaktowy osoby czym jest zainteresowana jakieś uwagi. Dodatkowo system działa z możliwością tworzenia ról i uprawnień dla pojedynczych użytkowników. Istnieje również możliwość założenia konta klienta (klient sam siebie dodaje do bazy systemu i ma możliwość jego zarządzaniem).


* spring boot
* testy metoda TDD
* gradle
* angular 2


![ThotSchemaV1.png](https://bitbucket.org/repo/Lynabz/images/2373104552-ThotSchemaV1.png)