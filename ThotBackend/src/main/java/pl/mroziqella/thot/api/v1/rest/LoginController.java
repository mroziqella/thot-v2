package pl.mroziqella.thot.api.v1.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mroziqella.thot.dto.LoginDTO;
import pl.mroziqella.thot.dto.UserDTO;
import pl.mroziqella.thot.service.AuthenticationService;

import javax.annotation.processing.SupportedOptions;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Mroziqella on 18.02.2017.
 */
@RestController
@RequestMapping("/")
public class LoginController {
    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping("api/1/login/test")
    public UserDTO hello() {
        return UserDTO.Builder.userDTO()
                .withEmail("test@o2.pl")
                .withFirtsName("dasd")
                .withLastName("dasd")
                .withStatus("Aktywny")
                .build();
    }


    @RequestMapping(value = "login",method = RequestMethod.POST)
    public UserDTO authenticate(@RequestBody LoginDTO loginDTO,HttpServletResponse resp){
        return authenticationService.authenticate(loginDTO);

    }

}
