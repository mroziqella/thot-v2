package pl.mroziqella.thot.dto;

/**
 * Created by Mroziqella on 18.02.2017.
 */
public class UserDTO {
    private Long id;
    private String email;
    private String firtsName;
    private String lastName;
    private String status;
    private String base64;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirtsName() {
        return firtsName;
    }

    public void setFirtsName(String firtsName) {
        this.firtsName = firtsName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public static class Builder {
        private String email;
        private String firtsName;
        private String lastName;
        private String status;
        private String base64;

        public static Builder userDTO() {
            return new Builder();
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withFirtsName(String firtsName) {
            this.firtsName = firtsName;
            return this;
        }

        public Builder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder withBase64(String base64) {
            this.base64 = base64;
            return this;
        }

        public UserDTO build() {
            UserDTO userDTO = new UserDTO();
            userDTO.setEmail(email);
            userDTO.setFirtsName(firtsName);
            userDTO.setLastName(lastName);
            userDTO.setStatus(status);
            userDTO.setBase64(base64);
            return userDTO;
        }
    }

}

