package pl.mroziqella.thot;

import org.hamcrest.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.Matchers;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.mroziqella.thot.api.v1.rest.LoginController;
import pl.mroziqella.thot.dto.LoginDTO;
import pl.mroziqella.thot.dto.UserDTO;
import pl.mroziqella.thot.service.AuthenticationService;

/**
 * Created by Mroziqella on 18.02.2017.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AuthenticationServiceTest {


    @Mock
    AuthenticationService authenticationService;
    @InjectMocks
    LoginController loginController;


    @Test
    public void authenticate(){
        Mockito.when(authenticationService.authenticate(Matchers.anyObject()))
                .thenReturn(UserDTO.Builder
                    .userDTO()
                    .withEmail("Mroziqella")
                    .withFirtsName("Kamil")
                    .withLastName("Mróz")
                    .build()
                );

        Assert.assertEquals(loginController.authenticate(new LoginDTO("Kamil","Mróz")).getFirtsName(),"Kamil");
        Mockito.verify(authenticationService,Mockito.times(1)).authenticate(Matchers.anyObject());

    }

}
