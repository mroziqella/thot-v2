import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule,JsonpModule  } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import {LoginComponent} from './login/login.component'
import {StartComponent} from "./start/start.component";
import {routing} from "./start.routes";
import {HomeModule} from "./home/home.module";
import {ContentComponent} from "./home/content/content.component";








@NgModule({
  declarations: [
    LoginComponent,
    StartComponent,
    HomeModule,
    ContentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule,
    routing
  ],
  providers: [],
  bootstrap: [StartComponent]
})


export class AppModule {
}
