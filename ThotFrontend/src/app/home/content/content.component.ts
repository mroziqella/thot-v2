import { Component } from '@angular/core';
import {TestService} from "../../service/serviceTest";

@Component({
  template: `
  		Testowy tekst
  		<div *ngFor="let item of date">
  		  {{item.name}}
        </div>
  `,
  providers:[TestService]
})
export class ContentComponent {
    date:any;


  constructor(private httpService:TestService) {
    this.getMessage();
  }

  getMessage(){
    this.httpService.getCurrentTime()
      .subscribe(
        data=>this.date = data,
        error=>alert(error),
        ()=>console.log("Finished")
      );
  }


}
