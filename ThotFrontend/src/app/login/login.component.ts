import { Component } from '@angular/core';
import {User} from "../domain/User";
import {LoginService} from "./service/loginService";
import {Router} from "@angular/router";
import {SessionService} from "./service/sessionService";


@Component({
  templateUrl: './login.html',
  providers:[LoginService,SessionService]
})
export class LoginComponent {
   user:User = new User();

  constructor(private httpService:LoginService,private router: Router) {

  }

  public sendLogin(){
    this.httpService.login(this.user)
      .subscribe(
        data=>{
          this.httpService.setSession(data);
          this.router.navigate(['home'])
        },
        error=>alert(error),
        ()=>console.log("Finished")
      );
  }

  public sendLoginTest(){
    this.httpService.loginTest(this.user)
      .subscribe(
        data=>{
          this.router.navigate(['home'])
        },
        error=>alert(error),
        ()=>console.log("Finished")
      );
  }



}
