
import {Injectable, forwardRef, Inject, Injector} from "@angular/core";
import {Headers, RequestOptions, Http, RequestMethod, Response} from '@angular/http';
import 'rxjs/Rx';
import {User} from "../../domain/User";
import {Observable} from "rxjs";
import { Cookie } from 'ng2-cookies/ng2-cookies';
import {SessionService} from "./sessionService";

@Injectable()
export class LoginService{
    constructor(private http: Http,private injector: Injector){
    }



  setSession(user:any){
    Cookie.set('auth', user.base64);
  }

    login(user:User):Observable<Response>{
      let body = JSON.stringify(user);
      //'Authorization':'Basic YmlsbDphYmMxMjM='
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http
        .post("http://localhost:8080/login",body, options).map(res=>res.json());


    }
  loginTest(user:User):Observable<Response>{
    let body = JSON.stringify(user);
    return this.http
      .get("http://localhost:8080/api/1/login/test", this.injector.get(SessionService).getOptions()).map(res=>res.json());


  }
}
