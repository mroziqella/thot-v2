
import {Injectable, Component} from "@angular/core";
import {Headers, RequestOptions, Http, RequestMethod, Response} from '@angular/http';
import 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()
export class SessionService{
    constructor(private http: Http){
    }

   getOptions(){
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':Cookie.get('auth')});
    let options = new RequestOptions({ headers: headers });
    return options;
  }
}
