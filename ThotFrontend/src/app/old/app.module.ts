import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { ContentComponent } from './content/content.component';
import { MenuComponent } from './menu/menu.component';
import { routing } from './routes/home.routes';




@NgModule({
  declarations: [
    HomeComponent,
	  UsersComponent,
    ContentComponent,
    MenuComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
	 routing
  ],
  providers: [],
  bootstrap: [ContentComponent,MenuComponent]
})


export class AppModule {
}
