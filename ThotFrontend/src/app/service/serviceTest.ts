
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class TestService{
    constructor(private http: Http){

    }

    getCurrentTime(){
        return this.http.get("https://jsonplaceholder.typicode.com/users")
          .map(res=>res.json())
    }
}
