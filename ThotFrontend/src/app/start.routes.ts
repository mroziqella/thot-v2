import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomeModule} from "./home/home.module";
import {ContentComponent} from "./home/content/content.component";




export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'home/index', pathMatch: 'full'
  },
  { path: 'home',component: HomeModule,
    children:[
      {path:'index', component: ContentComponent}
    ]
  }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
